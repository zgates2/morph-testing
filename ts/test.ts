import fs from "fs"
import * as path from "path" 

const data = new Date().toISOString()

fs.writeFileSync(path.join(__dirname, "test.file"), data)