const fs = require('fs')
const path = require('path')

const data = new Date().toISOString();

fs.writeFileSync(path.join(__dirname, "test.file"), data);