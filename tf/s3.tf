resource "aws_s3_bucket" "morph_test" {
  bucket = "morph-telus-2023"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}